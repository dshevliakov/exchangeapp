package com.dshevliakov.exchangeapp.exchange.model.response;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "Cube")
public class Cube {

    @Attribute(name = "time")
    private String time;

    @ElementList(inline = true)
    private List<Rate> exchangeRates;

    public String getTime() {
        return time;
    }

    public List<Rate> getExchangeRates() {
        return exchangeRates;
    }
}
