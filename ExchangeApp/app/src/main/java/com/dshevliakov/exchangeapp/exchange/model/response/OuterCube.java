package com.dshevliakov.exchangeapp.exchange.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Cube")
public class OuterCube {

    @Element(name = "Cube")
    private Cube cube;

    public Cube getCube() {
        return cube;
    }
}
