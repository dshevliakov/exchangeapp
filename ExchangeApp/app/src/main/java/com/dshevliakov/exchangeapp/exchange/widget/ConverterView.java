package com.dshevliakov.exchangeapp.exchange.widget;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.dshevliakov.exchangeapp.R;
import com.dshevliakov.exchangeapp.exchange.ExchangePagerAdapter;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Main view for converting
 * Contains two ViewPagers storing original and converted currencies and amounts
 */
public class ConverterView extends LinearLayout {

    public interface ConverterCallbacks {

        void onOriginalChanged(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency);

        void onConvertedChanged(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency);
    }

    private static final String STATE_ORIGINAL_CURRENCIES = "original_currencies";
    private static final String STATE_CONVERTED_CURRENCIES = "converted_currencies";

    private List<ExchangeCurrency> currencies;

    private ViewPager original;
    private ViewPager converted;

    private ExchangePagerAdapter originalAdapter;
    private ExchangePagerAdapter convertedAdapter;

    private ExchangeCurrencyView.OnExchangeValueChangedCallback originalConvertValueChanged = ()
            -> onOriginalChanged(original.getCurrentItem(), converted.getCurrentItem());
    private ExchangeCurrencyView.OnExchangeValueChangedCallback convertedConvertValueChanged = ()
            -> onConvertedChanged(original.getCurrentItem(), converted.getCurrentItem());

    private ConverterCallbacks converterCallbacks;

    public ConverterView(Context context) {
        this(context, null);
    }

    public ConverterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConverterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        setOrientation(VERTICAL);
        inflate(context, R.layout.view_layout_converter, this);
        setUpViews();
    }

    private void setUpViews() {
        original = (ViewPager) findViewById(R.id.converter_original_pager);
        CircleIndicator originalIndicator = (CircleIndicator) findViewById(R.id.converter_original_indicator);
        originalAdapter = new ExchangePagerAdapter(originalConvertValueChanged);
        original.setAdapter(originalAdapter);
        originalIndicator.setViewPager(original);
        originalAdapter.registerDataSetObserver(originalIndicator.getDataSetObserver());
        original.addOnPageChangeListener(new ConverterOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setConvertedCurrencies(new ArrayList<>(currencies));
                super.onPageSelected(position);
            }
        });

        converted = (ViewPager) findViewById(R.id.converter_converted_pager);
        CircleIndicator convertedIndicator = (CircleIndicator) findViewById(R.id.converter_converted_indicator);
        convertedAdapter = new ExchangePagerAdapter(convertedConvertValueChanged);
        converted.setAdapter(convertedAdapter);
        convertedIndicator.setViewPager(converted);
        convertedAdapter.registerDataSetObserver(convertedIndicator.getDataSetObserver());
        converted.addOnPageChangeListener(new ConverterOnPageChangeListener());
    }

    private ExchangeCurrency getOriginalCurrency(int position) {
        return originalAdapter.getItem(position);
    }

    private ExchangeCurrency getConvertedCurrency(int position) {
        return convertedAdapter.getItem(position);
    }

    private void onOriginalChanged(int originalPosition, int convertedPosition) {
        if (converterCallbacks != null) {
            converterCallbacks.onOriginalChanged(getOriginalCurrency(originalPosition), getConvertedCurrency(convertedPosition));
        }
    }

    private void onConvertedChanged(int originalPosition, int convertedPosition) {
        if (converterCallbacks != null) {
            converterCallbacks.onConvertedChanged(getOriginalCurrency(originalPosition), getConvertedCurrency(convertedPosition));
        }
    }

    private void setConvertedCurrencies(List<ExchangeCurrency> currencies) {
        List<ExchangeCurrency> copy = copyCurrencies(currencies);
        copy.remove(original.getCurrentItem());
        convertedAdapter.setConvertCurrencies(copy);
    }

    private List<ExchangeCurrency> copyCurrencies(List<ExchangeCurrency> currencies) {
        List<ExchangeCurrency> copy = new ArrayList<>(currencies.size());
        for (ExchangeCurrency c : currencies) {
            copy.add(c.copy());
        }
        return copy;
    }

    public void onRatesChanged() {
        onOriginalChanged(original.getCurrentItem(), converted.getCurrentItem());
    }

    public void setCurrencies(List<ExchangeCurrency> currencies) {
        this.currencies = currencies;
        originalAdapter.setConvertCurrencies(copyCurrencies(currencies));
        setConvertedCurrencies(currencies);
    }

    public void updateOriginalCurrency(ExchangeCurrency exchangeCurrency) {
        originalAdapter.updateCurrency(exchangeCurrency);
    }

    public void updateConvertedCurrency(ExchangeCurrency exchangeCurrency) {
        convertedAdapter.updateCurrency(exchangeCurrency);
    }

    public void setConverterCallbacks(ConverterCallbacks converterCallbacks) {
        this.converterCallbacks = converterCallbacks;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle savedState = new Bundle();
        savedState.putParcelable("super", super.onSaveInstanceState());
        savedState.putSerializable(STATE_ORIGINAL_CURRENCIES, (ArrayList<ExchangeCurrency>) originalAdapter.getItems());
        savedState.putSerializable(STATE_CONVERTED_CURRENCIES, (ArrayList<ExchangeCurrency>) convertedAdapter.getItems());
        return savedState;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Bundle savedState = (Bundle) state;
        originalAdapter.setConvertCurrencies((ArrayList<ExchangeCurrency>) savedState.getSerializable(STATE_ORIGINAL_CURRENCIES));
        convertedAdapter.setConvertCurrencies((ArrayList<ExchangeCurrency>) savedState.getSerializable(STATE_CONVERTED_CURRENCIES));
        super.onRestoreInstanceState(savedState.getParcelable("super"));
    }

    private class ConverterOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //do nothing
        }

        @Override
        public void onPageSelected(int position) {
            onOriginalChanged(original.getCurrentItem(), converted.getCurrentItem());
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            //do nothing
        }
    }
}
