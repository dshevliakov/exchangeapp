package com.dshevliakov.exchangeapp.exchange.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dshevliakov.exchangeapp.R;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ExchangeCurrencyView extends LinearLayout {

    public interface OnExchangeValueChangedCallback {
        void onConvertValueChanged();
    }

    private TextView name;
    private EditText value;

    private ExchangeCurrency exchangeCurrency;
    private OnExchangeValueChangedCallback exchangeValueChangedCallback;

    private NumberFormat numberFormat = new DecimalFormat();

    public ExchangeCurrencyView(Context context) {
        this(context, null);
    }

    public ExchangeCurrencyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExchangeCurrencyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        setOrientation(HORIZONTAL);
        inflate(getContext(), R.layout.view_layout_currency, this);
        setUpViews();
    }

    private void setUpViews() {
        name = (TextView) findViewById(R.id.exchange_currency_name);
        value = (EditText) findViewById(R.id.exchange_currency_value);
        value.setLongClickable(false);
    }

    private void setCurrencyName(String currencyName) {
        name.setText(currencyName);
    }

    private void setCurrencyValue(BigDecimal decimal) {
        value.removeTextChangedListener(valueWatcher);
        value.setText(numberFormat.format(decimal));
        value.addTextChangedListener(valueWatcher);
    }

    public void setConvertCurrencyAndUpdateView(ExchangeCurrency currency) {
        this.exchangeCurrency = currency;
        setCurrencyName(currency.getCurrency());
        setCurrencyValue(currency.getValue());
    }

    public void setExchangeValueChangedCallback(OnExchangeValueChangedCallback callback) {
        this.exchangeValueChangedCallback = callback;
    }

    private TextWatcher valueWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (exchangeValueChangedCallback != null) {
                if (s.length() == 0) {
                    exchangeCurrency.setValue(BigDecimal.ZERO);
                    s.append('0'); //if last symbol deleted
                } else {
                    exchangeCurrency.setValue(new BigDecimal(s.toString()));
                    exchangeValueChangedCallback.onConvertValueChanged();
                }
            }
        }
    };
}
