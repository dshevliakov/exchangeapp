package com.dshevliakov.exchangeapp.exchange.di;

import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrenciesStorage;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ExchangeModule {

    @Singleton
    @Provides
    ExchangeApi provideExchangeApi(Retrofit retrofit) {
        return retrofit.create(ExchangeApi.class);
    }

    @Singleton
    @Provides
    ExchangeCurrenciesStorage provideExchangeCurrenciesStorage() {
        return new ExchangeCurrenciesStorage();
    }
}
