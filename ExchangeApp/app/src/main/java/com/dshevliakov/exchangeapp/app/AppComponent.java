package com.dshevliakov.exchangeapp.app;

import com.dshevliakov.exchangeapp.exchange.di.ExchangeModule;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrenciesStorage;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeApi;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ExchangeModule.class})
public interface AppComponent {

    ExchangeApi exchangeApi();

    ExchangeCurrenciesStorage exchangeCurrenciesStorage();
}
