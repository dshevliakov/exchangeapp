package com.dshevliakov.exchangeapp.exchange;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;

import java.util.List;

/**
 * View state interface
 */
interface ExchangeView extends MvpView {

    /**
     * Called when initial currencies loaded
     *
     * @param currencies initial currencies
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void onCurrenciesLoaded(List<ExchangeCurrency> currencies);

    /**
     * Called when exchange rates were loaded
     */
    @StateStrategyType(SkipStrategy.class)
    void onRatesLoaded();

    /**
     * Called when original amount or currency has changed
     *
     * @param converted conversion result
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void onConvertedFromOriginal(ExchangeCurrency converted);

    /**
     * Called when converted amount or currency has changed
     *
     * @param original conversion result
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void onConvertedFromConverted(ExchangeCurrency original);

    /**
     * Called if there is problem while converting currencies
     */
    @StateStrategyType(SkipStrategy.class)
    void onConversionFailed();

    /**
     * Called when there is problem while loading exchange rates
     */
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onError();
}
