package com.dshevliakov.exchangeapp.exchange.exception;

public class ConversionException extends Exception {

    public ConversionException(String message) {
        super(String.format("Conversion failed because %s", message));
    }
}
