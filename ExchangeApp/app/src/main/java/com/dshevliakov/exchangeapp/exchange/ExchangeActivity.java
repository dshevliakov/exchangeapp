package com.dshevliakov.exchangeapp.exchange;

import android.os.Bundle;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.dshevliakov.exchangeapp.R;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;
import com.dshevliakov.exchangeapp.exchange.widget.ConverterView;

import java.util.List;

public class ExchangeActivity extends MvpAppCompatActivity implements ExchangeView {

    @InjectPresenter
    ExchangePresenter exchangePresenter;

    private ConverterView converter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        converter = (ConverterView) findViewById(R.id.activity_exchange_converter);
        converter.setConverterCallbacks(new ConverterView.ConverterCallbacks() {
            @Override
            public void onOriginalChanged(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency) {
                exchangePresenter.convertFromOriginalCurrency(originalCurrency, convertedCurrency);
            }

            @Override
            public void onConvertedChanged(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency) {
                exchangePresenter.convertFromConvertedCurrency(originalCurrency, convertedCurrency);
            }
        });
    }

    @Override
    public void onCurrenciesLoaded(List<ExchangeCurrency> currencies) {
        converter.setCurrencies(currencies);
    }

    @Override
    public void onRatesLoaded() {
        converter.onRatesChanged();
    }

    @Override
    public void onConvertedFromOriginal(ExchangeCurrency converted) {
        converter.updateConvertedCurrency(converted);
    }

    @Override
    public void onConvertedFromConverted(ExchangeCurrency original) {
        converter.updateOriginalCurrency(original);
    }

    @Override
    public void onConversionFailed() {
        Toast.makeText(this, R.string.error_conversion_failed, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError() {
        Toast.makeText(this, R.string.error_currencies_load, Toast.LENGTH_LONG).show();
    }
}
