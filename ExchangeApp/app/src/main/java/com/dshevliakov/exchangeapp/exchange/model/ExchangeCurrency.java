package com.dshevliakov.exchangeapp.exchange.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Object stores currency and amount for convert
 */
public class ExchangeCurrency implements Serializable {

    public static final String EUR_CURRENCY = "EUR";
    public static final String USD_CURRENCY = "USD";
    public static final String GBP_CURRENCY = "GBP";

    private String currency;

    private BigDecimal value;

    public ExchangeCurrency(String currency) {
        this.currency = currency;
        this.value = BigDecimal.ZERO;
    }

    private ExchangeCurrency(String currency, BigDecimal value) {
        this.currency = currency;
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public ExchangeCurrency copy() {
        return new ExchangeCurrency(currency, value);
    }
}
