package com.dshevliakov.exchangeapp.app;

import android.app.Application;

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        buildAppComponent();
    }

    private void buildAppComponent() {
        appComponent = DaggerAppComponent
                .builder()
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
