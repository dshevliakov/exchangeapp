package com.dshevliakov.exchangeapp.exchange.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Storage that provides initial {@link ExchangeCurrency} list
 */
public class ExchangeCurrenciesStorage {

    public List<ExchangeCurrency> getCurrencies() {
        List<ExchangeCurrency> currencies = new ArrayList<>(3);
        currencies.add(new ExchangeCurrency(ExchangeCurrency.EUR_CURRENCY));
        currencies.add(new ExchangeCurrency(ExchangeCurrency.GBP_CURRENCY));
        currencies.add(new ExchangeCurrency(ExchangeCurrency.USD_CURRENCY));
        return currencies;
    }
}
