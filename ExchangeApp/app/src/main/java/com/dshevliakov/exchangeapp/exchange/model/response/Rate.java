package com.dshevliakov.exchangeapp.exchange.model.response;

import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "Cube")
public class Rate {

    @Attribute(name = "currency")
    private String currency;

    @Attribute(name = "rate")
    private double rate;

    public static Rate createEur() {
        return new Rate(ExchangeCurrency.EUR_CURRENCY, 1d);
    }

    public Rate() {

    }

    public Rate(String currency, double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public String getCurrency() {
        return currency;
    }

    public double getRate() {
        return rate;
    }
}
