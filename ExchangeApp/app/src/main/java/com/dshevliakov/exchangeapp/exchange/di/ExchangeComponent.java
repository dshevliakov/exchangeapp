package com.dshevliakov.exchangeapp.exchange.di;

import com.dshevliakov.exchangeapp.app.AppComponent;
import com.dshevliakov.exchangeapp.base.di.ViewScope;
import com.dshevliakov.exchangeapp.exchange.ExchangePresenter;

import dagger.Component;

@ViewScope
@Component(dependencies = {AppComponent.class})
public interface ExchangeComponent {

    void inject(ExchangePresenter presenter);
}
