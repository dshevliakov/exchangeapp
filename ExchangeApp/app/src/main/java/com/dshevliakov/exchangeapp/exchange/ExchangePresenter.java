package com.dshevliakov.exchangeapp.exchange;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.dshevliakov.exchangeapp.app.App;
import com.dshevliakov.exchangeapp.exchange.di.DaggerExchangeComponent;
import com.dshevliakov.exchangeapp.exchange.exception.ConversionException;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeApi;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrenciesStorage;
import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;
import com.dshevliakov.exchangeapp.exchange.model.response.Rate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@InjectViewState
public class ExchangePresenter extends MvpPresenter<ExchangeView> {

    private static final long EXCHANGE_RATES_REQUEST_PERIOD_IN_SECONDS = 30;

    @Inject
    ExchangeApi api;

    @Inject
    ExchangeCurrenciesStorage currencyStorage;

    private Subscription exchangeRatesSubscription;

    private HashMap<String, Rate> rates;

    ExchangePresenter() {
        DaggerExchangeComponent
                .builder()
                .appComponent(App.getAppComponent())
                .build()
                .inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadCurrencies();
        loadRates();
    }

    private void loadCurrencies() {
        getViewState().onCurrenciesLoaded(currencyStorage.getCurrencies());
    }

    private void loadRates() {
        if (exchangeRatesSubscription == null || exchangeRatesSubscription.isUnsubscribed()) {

            exchangeRatesSubscription = Observable.interval(0, EXCHANGE_RATES_REQUEST_PERIOD_IN_SECONDS, TimeUnit.SECONDS)
                    .flatMap(interval
                            -> Observable.merge(Observable.just(Rate.createEur()), getLoadRatesObservable())
                            .filter(rate -> {
                                String currency = rate.getCurrency();
                                return ExchangeCurrency.EUR_CURRENCY.equals(currency) ||
                                        ExchangeCurrency.GBP_CURRENCY.equals(currency) ||
                                        ExchangeCurrency.USD_CURRENCY.equals(currency);
                            })
                            .toMap(Rate::getCurrency))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(ratesMap -> {
                                rates = new HashMap<>(ratesMap);
                                getViewState().onRatesLoaded();
                            },
                            throwable -> getViewState().onError());
        }
    }

    private Observable<Rate> getLoadRatesObservable() {
        return api.getExchangeRates()
                .flatMap(exchangeResponse -> Observable.from(exchangeResponse.getOuterCube().getCube().getExchangeRates()));
    }

    private ExchangeCurrency convert(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency) throws ConversionException {
        if (rates == null) {
            throw new ConversionException("Exchange Rates is null");
        } else {
            Rate fromRate = rates.get(originalCurrency.getCurrency());
            Rate toRate = rates.get(convertedCurrency.getCurrency());

            if (fromRate == null || toRate == null) {
                throw new ConversionException("Can't find rates for currencies");
            } else {
                BigDecimal convertedValue = originalCurrency.getValue()
                        .divide(BigDecimal.valueOf(fromRate.getRate()), RoundingMode.HALF_DOWN)
                        .multiply(BigDecimal.valueOf(toRate.getRate()));
                convertedCurrency.setValue(convertedValue);
            }
        }
        return convertedCurrency;
    }

    void convertFromOriginalCurrency(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency) {
        try {
            getViewState().onConvertedFromOriginal(convert(originalCurrency, convertedCurrency));
        } catch (ConversionException e) {
            getViewState().onConversionFailed();
        }
    }

    void convertFromConvertedCurrency(ExchangeCurrency originalCurrency, ExchangeCurrency convertedCurrency) {
        try {
            getViewState().onConvertedFromConverted(convert(convertedCurrency, originalCurrency));
        } catch (ConversionException e) {
            getViewState().onConversionFailed();
        }
    }

    @Override
    public void onDestroy() {
        exchangeRatesSubscription.unsubscribe();
        super.onDestroy();
    }
}
