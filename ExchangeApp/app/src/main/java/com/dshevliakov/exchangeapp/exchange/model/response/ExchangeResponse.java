package com.dshevliakov.exchangeapp.exchange.model.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "gesmes:Envelope")
public class ExchangeResponse {

    @Element(name = "Cube")
    private OuterCube outerCube;

    public OuterCube getOuterCube() {
        return outerCube;
    }
}
