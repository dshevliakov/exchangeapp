package com.dshevliakov.exchangeapp.exchange.model;

import com.dshevliakov.exchangeapp.exchange.model.response.ExchangeResponse;

import retrofit2.http.GET;
import rx.Observable;

public interface ExchangeApi {

    @GET("stats/eurofxref/eurofxref-daily.xml")
    Observable<ExchangeResponse> getExchangeRates();
}
