package com.dshevliakov.exchangeapp.exchange;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.dshevliakov.exchangeapp.exchange.model.ExchangeCurrency;
import com.dshevliakov.exchangeapp.exchange.widget.ExchangeCurrencyView;

import java.util.ArrayList;
import java.util.List;

public class ExchangePagerAdapter extends PagerAdapter {

    private List<ExchangeCurrency> currencies;

    private ExchangeCurrencyView.OnExchangeValueChangedCallback convertValueChangedCallback;

    public ExchangePagerAdapter(ExchangeCurrencyView.OnExchangeValueChangedCallback callback) {
        currencies = new ArrayList<>();
        convertValueChangedCallback = callback;
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof ExchangeCurrencyView) {
            ExchangeCurrencyView view = (ExchangeCurrencyView) object;
            int position = (int) view.getTag();
            if (position < currencies.size()) { //if position is in bounds then update view
                view.setConvertCurrencyAndUpdateView(getItem(position));
                return POSITION_UNCHANGED;
            }
        }
        return POSITION_NONE; //remove/instantiate otherwise
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ExchangeCurrencyView view = new ExchangeCurrencyView(container.getContext());
        view.setExchangeValueChangedCallback(convertValueChangedCallback);
        view.setConvertCurrencyAndUpdateView(getItem(position));
        view.setTag(position); //set position as tag to use it in getItemPosition()
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public List<ExchangeCurrency> getItems() {
        return currencies;
    }

    public ExchangeCurrency getItem(int position) {
        return currencies.get(position);
    }

    public void updateCurrency(ExchangeCurrency exchangeCurrency) {
        for (ExchangeCurrency c : currencies) {
            if (c.getCurrency().equals(exchangeCurrency.getCurrency())) {
                c.setValue(exchangeCurrency.getValue());
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void setConvertCurrencies(List<ExchangeCurrency> currencies) {
        this.currencies.clear();
        this.currencies.addAll(currencies);
        notifyDataSetChanged();
    }
}
